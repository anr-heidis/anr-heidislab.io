## Welcome to the HEIDIS website.

HEIDIS is a french research project sponsored by the ANR (Agence Nationale de la Recherche).

It proposes through a set of experiments and evaluations, oriented towards the open source solutions and community, the early implementation of 5/6G softwarized radio-communication principles.

HEIDIS is leaded by Université Paris Saclay, and welcomes as partners le Conservatoire National des Arts et Métiers (CNAM), Orange Labs, EDF R&D and SMILE.
